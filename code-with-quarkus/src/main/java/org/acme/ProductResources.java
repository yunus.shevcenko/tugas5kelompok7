package org.acme;


import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.*;

@Path("/product")
public class ProductResources {

//    List<Product> products = new ArrayList<>();

    @Inject
    Validator validator;
    HashMap<Integer, Product> products = new HashMap<Integer, Product>();
    public static Integer id = 1;


    @GET
    public HashMap<Integer, Product> getProducts() {
        return products;
    }

    @GET
    @Path("{id}")
    public Product getProductByIndex(@PathParam("id") Integer id) {

        return products.get(id);
    }


    @POST
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
    public Result addProduct(Product product) {
        Set<ConstraintViolation<Product>> violations = validator.validate(product);
        if (violations.isEmpty()) {
            products.put(id++, product);
            return new Result("Product " + product.name + " berhasil ditambahkan");
        }else {
            return new Result(violations);
        }
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Product editProduct(@PathParam("id") Integer index, Product newProduct) {
        products.replace(index, newProduct);
        return newProduct;
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String deleteProduct(@PathParam("id") Integer id) {
        products.remove(id.intValue());
        return "Product berhasil dihapus";
    }



}